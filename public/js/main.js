// database reference pada firebase
let REFERENCE_STRING = "data";
// format waktu
let DATE_TIME_FORMAT = "#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#";

// variabel untuk tabel
let mTable;

function start(myTable){
    // let dbRef = firebase.database().ref(REFERENCE_STRING);
    //mTable = myTable;
    // dbRef.on('value',getData, errorData);
    
    mTable = myTable;
    getMyData();
}

function getData(snap){
    mTable.clear().draw();
    let progressBar =$('#progress');
    progressBar.show();

    let data = snap.val();
    let keys = Object.keys(data);
    console.log(data);
    for (let i = 0; i<keys.length;i++){
        let k = keys[i];
        let nama = data[k].nama;
        let noHp = data[k].noHp;
        let hasilTest = data[k].hasilTest;
        let timestamp = data[k].timestamp;
        let waktu = convertMillisToDate(timestamp);
        
        let dataSet = [i+1,nama,noHp,hasilTest,waktu];
        mTable.rows.add([dataSet]).draw();
    }
    progressBar.hide()
}
class HasilTes{
    constructor(nomor,nama,nomortelpon,hasiltes,timestamp){
        this.nomor=nomor;
        this.nama=nama;
        this.nomortelpon=nomortelpon;
        this.hasilTest=hasiltes,
        this.timestamp=timestamp;
    } 
}

function errorData(error){
    console.log("Error: "+error);
}

function convertMillisToDate(millis){
    return new Date(millis).toLocaleString();
}
function getMyData(){
    mTable.clear().draw();
    let progressBar =$('#progress');
    progressBar.show();
    const myDatas=[];
    for(let i=0;i<10;i++){
        myDatas[i]=new HasilTes(i,"Nama"+i,"00"+i,"Buta warna total",i*123456789);
    }
    myDatas.forEach(myData => {
        let dataset=[myData.nomor,myData.nama,myData.nomortelpon,myData.hasilTest,myData.timestamp];
        mTable.rows.add([dataset]).draw();  
    });
    progressBar.hide();
}
function ubahCSS(){
    $("button").attr("class",(indexOfSelectedElement,oldValue)=>{
        if(indexOfSelectedElement==0) oldValue+=" btn btn-secondary btn-sm";
        if(indexOfSelectedElement==1) oldValue+=" btn btn-success btn-sm";
        if(indexOfSelectedElement==2) oldValue+=" btn btn-danger btn-sm";
        return oldValue;
        
    })
}